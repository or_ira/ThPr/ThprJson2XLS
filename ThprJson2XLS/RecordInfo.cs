﻿using System;

namespace ThprJson2XLS {
  class RecordInfo {
    public Guid Id { get; set; }
    public string SheetName { get; set; }
    public int Row { get; set; }
    public int Column { get; set; }
    public string Code { get; set; }
    public Item Source { get; set; }
  }
}
