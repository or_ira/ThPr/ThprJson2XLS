﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace ThprJson2XLS {
  class Item {
    public Guid Id { get; set; }
    public TypeName TypeName { get; set; } = TypeName.Node;

    public string Code { get; set; }
    public string Name { get; set; }
    /// <summary>
    /// Источник данных, на основе которых заполняются некоторые свойства (Id, Name, TypeName, ParentId).
    /// </summary>
    public JObject Source { get; set; }

    /// <summary>
    /// Идентификатор родительского нода, находящегося на один уровень выше.
    /// </summary>
    public Guid? ParentId { get; set; } = null;
    /// <summary>
    /// Элементы на том же самом уровне, от которых непосредственно тянется связь с текущим элементом.
    /// </summary>
    public HashSet<Item> PreviousItems { get; set; } = new HashSet<Item>();
    /// <summary>
    /// Элементы на том же самом уровне, к которым тянется связь непосредственно от текущего элемента
    /// </summary>
    public HashSet<Item> NextItems { get; set; } = new HashSet<Item>();

    /// <summary>
    /// Дочерние элементы, расположенные на один уровень ниже
    /// </summary>
    public HashSet<Item> ChildItems { get; set; } = new HashSet<Item>();

    public string GetFullCodePrefix(Dictionary<Guid, Item> dict) {
      if (ParentId.HasValue) {
        var parent = dict[ParentId.Value];
        string parentPrefix = $"{dict[ParentId.Value].GetFullCodePrefix(dict)}";

        if (!String.IsNullOrEmpty(parentPrefix)) parentPrefix = $"{parentPrefix}.{parent.Name}";
        else parentPrefix = parent.Name;

        return parentPrefix;
      }
      else {
        return "";
      }
    }

    public int GetLevelIndex(Dictionary<Guid, Item> dict) {
      if (!ParentId.HasValue) return 0;
      else {
        var parent = dict[ParentId.Value];
        return 1 + parent.GetLevelIndex(dict);
      }
    }
  }
}
