﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using OfficeOpenXml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ThprJson2XLS {

  class Program {
    private static JToken markers = null;

    static Dictionary<Guid, RecordInfo> mapCode = new Dictionary<Guid, RecordInfo>();

    static void Main(string[] args) {

      if (null == args) {
        Console.WriteLine("Вы не указали JSON-файлы, для которых следует " +
                          "сгенерировать соответствующие XLSX-файлы.");
        return;
      }

      foreach (var fileName in args) {

        var jsonFile = new FileInfo(Environment.ExpandEnvironmentVariables(fileName));
        var jsonObj = File.ReadAllText(jsonFile.FullName);
        JObject json = JsonConvert.DeserializeObject(jsonObj) as JObject;

        var dbFileName = Path.Combine(jsonFile.DirectoryName, Path.GetFileNameWithoutExtension(
          jsonFile.FullName) + ".database.json");

        var jsonFile2 = new FileInfo(Environment.ExpandEnvironmentVariables(dbFileName));
        var jsonObj2 = File.ReadAllText(jsonFile2.FullName);
        JObject json2 = JsonConvert.DeserializeObject(jsonObj2) as JObject;
        markers = json2["Markers"];


        var items = json["DbObjects"];

        // Общий словарь всех объектов
        Dictionary<Guid, Item> dict = new Dictionary<Guid, Item>();

        // Только те объекты, у которых свойство ParentId равно null
        Dictionary<Guid, Item> roots = new Dictionary<Guid, Item>();

        // Все объекты сохраняем в словарь
        foreach (var obj in items) {
          Item item = new Item();

          item.Id = Guid.Parse(obj["Id"].Value<string>());
          string _parentId = obj["ParentId"]?.Value<string>();
          item.ParentId = _parentId == null ? null : (Guid?)Guid.Parse(_parentId);

          item.Source = obj as JObject;
          item.TypeName = (TypeName)Enum.Parse(typeof(TypeName), obj["TypeName"].Value<string>());

          var propName = item.TypeName == TypeName.Connection || item.TypeName == TypeName.Variant ?
            "_text" : "_name";
          var jsonData = JsonConvert.DeserializeObject(item.Source["JsonData"].Value<string>()) as JObject;
          item.Name = jsonData[propName]?.Value<string>();
          item.Code = ""; //item.Name;

          dict.Add(item.Id, item);

          // В словаре roots храним элементы, не имеющие ссылку на родителя
          if (!item.ParentId.HasValue) roots.Add(item.Id, item);
        }

        // Теперь добавляем в объекты информацию о порядке их следования в цепочке на их уровне
        foreach (var item in dict.Values.Where(n => n.TypeName == TypeName.Connection)) {
          var jsonData = JsonConvert.DeserializeObject(item.Source["JsonData"].Value<string>()) as JObject;
          var source = dict[Guid.Parse(jsonData["_sourceID"].Value<string>())];
          var target = dict[Guid.Parse(jsonData["_targetID"].Value<string>())];

          source.NextItems.Add(target);
          target.PreviousItems.Add(source);
        }

        // Заполняем свойство FirstChildItems (нужны только Node)
        foreach (var item in dict.Values) {
          if (item.ParentId.HasValue) {
            var parent = dict[item.ParentId.Value];
            if (item.PreviousItems.Count == 0 || item.PreviousItems.All(n => n.TypeName != TypeName.Variant)) {
              parent.ChildItems.Add(item);
            }
          }
        }

        using (ExcelPackage p = new ExcelPackage()) {
          var name = Path.GetFileNameWithoutExtension(jsonFile.FullName);
          var xlsFile = new FileInfo(Path.Combine(jsonFile.DirectoryName ?? "", name + ".xlsx"));

          foreach (var root in roots.Values) {
            mapCode.Add(root.Id, new RecordInfo {
              Id = root.Id, Code = root.Name, Source = root, Column = 1, Row = 1,
              SheetName = currentSheetName
            });
          }

          foreach (var root in roots.Values) {

            foreach (var node in root.ChildItems.Where(n => n.TypeName == TypeName.Node)) {
              var sheetName = node.Name;
              ExcelWorksheet ws = p.Workbook.Worksheets.Add(sheetName);
              currentSheetName = ws.Name; // Длинное имя обрезается
              ws.Cells.Style.Font.Size = 11;
              ws.Cells.Style.Font.Name = "Calibri";

              mapCode.Add(node.Id, new RecordInfo {
                Id = node.Id, Code = node.Name, Source = node, Column = 1, Row = 1,
                SheetName = currentSheetName
              });

              // Поправка для значений ширины колонок в MS Excel (чтобы фактическое 
              // числовое значение ширины колонки (в "попугаях" от Майкрософт) было как 
              // можно ближе к тому числу, которое мы указываем в свойстве Width)
              double parrots = 0.710625;
              double baseWidth = 8.5;

              var headers = new[] {
                new { Value ="Стадия", Width = baseWidth * 1 },
                new { Value ="Раздел", Width = baseWidth * 1.5 },
                new { Value ="Шифр блока", Width = baseWidth * 1.7 },
                new { Value ="Блок", Width = baseWidth * 3 },
                new { Value ="Роль блока", Width = baseWidth * 1.5 },
                new { Value ="Условие", Width = baseWidth * 3 },
                new { Value ="Вариант условия", Width = baseWidth * 1.2 },
                new { Value ="Шифр этапа", Width = baseWidth * 1.7 },
                new { Value ="Этап", Width = baseWidth * 3 },
                new { Value ="Роль этапа", Width = baseWidth * 1.5 },
                new { Value ="ПО Этапа", Width = baseWidth * 2 },
                new { Value ="Вход этапа", Width = baseWidth * 3 },
                new { Value ="Выход этапа", Width = baseWidth * 3 },
                new { Value ="Задачи", Width = baseWidth * 3 }
              };

              {
                int i = 1;
                foreach (var item in headers) {
                  ws.Cells[1, i].Value = item.Value;
                  ws.Column(i++).Width = item.Width + parrots;

                }

                ws.Row(1).Style.Font.Bold = true;
                ws.View.FreezePanes(2, 16);
              }

              int rowIndex = 2;

              foreach (var childNode in node.ChildItems.Where(n => n.TypeName == TypeName.Node)) {
                rowIndex = BuildPhase(dict, childNode, rowIndex, ws);
              }
            }
          }

          foreach (var pair in mapCode) {
            WriteDependencies(dict, pair.Value, p.Workbook.Worksheets);
          }

          p.SaveAs(xlsFile);
        }
      }
    }

    private static string currentSheetName;

    public static string TrimFullCodePrefix(string fullCode) {
      var tmp = fullCode.Split('.').Skip(2);
      var result = String.Join(".", tmp);
      return result;
    }

    private static int BuildPhase(Dictionary<Guid, Item> dict, Item phase, int rowIndex, ExcelWorksheet ws) {
      var fullCode = phase.GetFullCodePrefix(dict) + $".{phase.Name}";
      var result = TrimFullCodePrefix(fullCode);
      mapCode.Add(phase.Id, new RecordInfo {
        Id = phase.Id, Code = result, Source = phase, Column = 1, Row = rowIndex,
        SheetName = currentSheetName
      });

      ws.Cells[rowIndex, 1].Value = result;

      foreach (var item in phase.ChildItems.Where(n => n.TypeName == TypeName.Node)) {
        rowIndex = BuildSection(dict, item, ++rowIndex, ws);
      }

      return ++rowIndex;
    }

    private static int BuildSection(Dictionary<Guid, Item> dict, Item section, int rowIndex, ExcelWorksheet ws) {
      var fullCode = section.GetFullCodePrefix(dict) + $".{section.Name}";
      var result = TrimFullCodePrefix(fullCode);
      mapCode.Add(section.Id, new RecordInfo {
        Id = section.Id, Code = result, Source = section, Column = 2, Row = rowIndex,
        SheetName = currentSheetName
      });

      ws.Cells[rowIndex, 2].Value = result;

      int index = 1;
      ++rowIndex;

      foreach (var item in section.ChildItems.Where(n => n.TypeName == TypeName.Node)) {
        rowIndex = BuildBlock(dict, item, rowIndex, ws, index++);
      }
      return rowIndex;
    }

    private static int BuildBlock(Dictionary<Guid, Item> dict, Item block, int rowIndex,
      ExcelWorksheet ws, int blockIndex) {
      var fullCode = block.GetFullCodePrefix(dict) + $".{blockIndex}";
      var result = TrimFullCodePrefix(fullCode);
      mapCode.Add(block.Id, new RecordInfo {
        Id = block.Id, Code = result, Source = block, Column = 3, Row = rowIndex,
        SheetName = currentSheetName
      });
      ws.Cells[rowIndex, 3].Value = result;
      ws.Cells[rowIndex, 4].Value = block.Name;

      {
        var _rowIndex = rowIndex;
        string text = block.Source["JsonData"].Value<string>();

        JObject jsonData = JsonConvert.DeserializeObject(block.Source["JsonData"].Value<string>()) as JObject;

        var roles = jsonData["_roles"];

        if (null != roles && roles.Any()) {
          StringBuilder sb1 = new StringBuilder();
          foreach (var record in roles) {
            var _value = record.Value<string>();
            var expandedValue = markers[_value]?.Value<string>();
            sb1.Append($"{expandedValue ?? _value}, ");
          }

          string value = sb1.ToString();
          value = value.Substring(0, value.Length - 2); // Убираем последний "; "
          ws.Cells[_rowIndex, 5].Value = value;
        }
      }

      ++rowIndex;

      int index = 1;

      foreach (var item in block.ChildItems) {
        if (item.TypeName == TypeName.Condition) {
          rowIndex = BuildCondition(dict, item, rowIndex, ws, result, ref index);
        }
        else if (item.TypeName == TypeName.Node) {
          rowIndex = BuildStage(dict, item, rowIndex, ws, result, ref index);
        }
        else { }
      }

      return rowIndex;
    }

    private static int BuildCondition(Dictionary<Guid, Item> dict, Item condition, int rowIndex, ExcelWorksheet ws,
      string code, ref int _index) {

      mapCode.Add(condition.Id, new RecordInfo {
        Id = condition.Id, Code = "", Source = condition, Column = 6, Row = rowIndex,
        SheetName = currentSheetName
      });

      ws.Cells[rowIndex, 6].Value = condition.Name;
      ++rowIndex;
      foreach (var item in condition.ChildItems) {
        rowIndex = BuildVariant(dict, item, rowIndex, ws, code, ref _index);
      }
      ws.Cells[--rowIndex, 6].Value = "[КОНЕЦ УСЛОВИЯ]";
      return rowIndex + 2;
    }

    private static int BuildVariant(Dictionary<Guid, Item> dict, Item variant, int rowIndex, ExcelWorksheet ws,
      string code, ref int _index) {

      mapCode.Add(variant.Id, new RecordInfo {
        Id = variant.Id, Code = "", Source = variant, Column = 7, Row = rowIndex,
        SheetName = currentSheetName
      });

      ws.Cells[rowIndex, 7].Value = variant.Name;

      foreach (var item in variant.NextItems) {
        rowIndex = BuildStage(dict, item, rowIndex, ws, code, ref _index);
        ++rowIndex;
      }
      return rowIndex;
    }

    //private static string GetRootName(Dictionary<Guid, Item> dict, Item item) {
    //  if (null == item || null == item.ParentId) return null;
    //  var parent = dict[item.ParentId.Value];
    //  if (null == parent.ParentId) return item.Name;
    //  else return GetRootName(dict, parent);
    //}

    private static int BuildStage(Dictionary<Guid, Item> dict, Item stage, int rowIndex, ExcelWorksheet ws,
      string code, ref int _index) {

      string _code = $"{code}.{_index++}";
      mapCode.Add(stage.Id, new RecordInfo {
        Id = stage.Id, Code = _code, Source = stage, Column = 8, Row = rowIndex,
        SheetName = currentSheetName
      });
      ws.Cells[rowIndex, 8].Value = _code;
      ws.Cells[rowIndex, 9].Value = stage.Name;

      int index = 1;
      StringBuilder sb = new StringBuilder();
      foreach (var item in stage.ChildItems.Where(n => !String.IsNullOrEmpty(n.Name))) {

        mapCode.Add(item.Id, new RecordInfo {
          Id = item.Id, Code = $"{_code}.{ index}", Source = item, Column = 14, Row = rowIndex,
          SheetName = currentSheetName
        });

        if (item.TypeName == TypeName.Node)
        {
          sb.AppendLine($"{index++}.{item.Name}");
        }
        else if (item.TypeName == TypeName.Condition)
        {
          sb.AppendLine($"УСЛОВИЕ: {item.Name}");
          foreach (var variant in item.ChildItems.Where(n => !String.IsNullOrEmpty(n.Name)))
          {
            sb.AppendLine($"ВАРИАНТ: {variant.Name}");
            foreach (var task in variant.NextItems.Where(n => !String.IsNullOrEmpty(n.Name))) {
              sb.AppendLine($"ЗАДАЧА: {task.Name}");
            }
          }
          sb.AppendLine($"[КОНЕЦ УСЛОВИЯ]");
        }
        else
        {

        }
      }
      ws.Cells[rowIndex, 14].Value = sb.ToString();

      JObject jsonData = JsonConvert.DeserializeObject(stage.Source["JsonData"].Value<string>()) as JObject;

      var programs = jsonData["_programs"];

      if (null != programs && programs.Any()) {
        StringBuilder sb1 = new StringBuilder();
        foreach (var record in programs) {
          var _value = record.Value<string>();
          var expandedValue = markers[_value]?.Value<string>();
          sb1.Append($"{expandedValue ?? _value}, ");
        }

        string value = sb1.ToString();
        value = value.Substring(0, value.Length - 2); // Убираем последний "; "
        ws.Cells[rowIndex, 11].Value = value;
      }

      var roles = jsonData["_roles"];

      if (null != roles && roles.Any()) {
        StringBuilder sb1 = new StringBuilder();
        foreach (var record in roles) {
          var _value = record.Value<string>();
          var expandedValue = markers[_value]?.Value<string>();
          sb1.Append($"{expandedValue ?? _value}, ");
        }

        string value = sb1.ToString();
        value = value.Substring(0, value.Length - 2); // Убираем последний "; "
        ws.Cells[rowIndex, 10].Value = value;
      }

      return ++rowIndex;
    }

    static void WriteDependencies(Dictionary<Guid, Item> dict, RecordInfo ri, ExcelWorksheets wss) {
      JObject jsonData = JsonConvert.DeserializeObject(ri.Source.Source["JsonData"].Value<string>()) as JObject;

      var isProductFor = jsonData["_isProductFor"];
      var ws = wss[ri.SheetName];

      if (ri.Source.TypeName != TypeName.Node || ri.Row == 1) return;

      if (null != isProductFor) {
        StringBuilder sb1 = new StringBuilder();
        foreach (var record in isProductFor) {
          Guid id = Guid.Parse(record["targetId"].Value<string>());
          string name = dict[id].Name;
          //var root = GetRootName(dict, dict[id]);
          var root = mapCode[id].Code;
          sb1.Append($"{ri.SheetName}: {root} {name}");
          string text = record["text"].Value<string>();
          if (!string.IsNullOrEmpty(text)) {
            sb1.Append($" ({text})");
          }
          sb1.Append($"; ");
        }

        string value = sb1.ToString();
        value = value.Substring(0, value.Length - 2); // Убираем последний "; "
        ws.Cells[ri.Row, 13].Value = value;
      }

      var dependsOn = jsonData["_dependsOn"];
      if (null != dependsOn) {
        StringBuilder sb1 = new StringBuilder();
        foreach (var record in dependsOn) {
          Guid id = Guid.Parse(record["productId"].Value<string>());
          string name = dict[id].Name;
          // var root = GetRootName(dict, dict[id]);
          var root = mapCode[id].Code;
          sb1.Append($"{ri.SheetName}: {root} {name}; ");

          string value = sb1.ToString();
          value = value.Substring(0, value.Length - 2); // Убираем последний "; "
          ws.Cells[ri.Row, 12].Value = value;
        }
      }
    }
  }
}